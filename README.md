# number2words
-----------------------

number2words is a Java 8+ library created to convert numbers to their word representations.

## Features
--------

* Converts an `Integer` to `String` words, with representations like:
- Any value between Integer.MIN_VALUE and Integer.MAX_VALUE
- Integers with String representations (e.g "1,234,567", "-123,456")

## Quick Start
1. Make sure your local environment is prepared with Java 1.8+ and maven
2. Clone repository and enter it.

```bash
git clone https://gitlab.com/frank3dev/number2words.git
cd number2words
```
3. Build

```bash
mvn clean package
```  
3. Run

```bash  
java -cp target/number2words-0.0.1-SNAPSHOT.jar com.sonatype.demos.number2words.NumberToWordsImp 123456
```

### How to use it as a dependency
```java
NumberToWords ntw = new NumberToWordsImp(10583);
String numberAsWords = ntw.convert();
assertEquals("Ten thousand five hundred and eighty three", numberAsWords);
```
