package com.sonatype.demos.number2words;

public interface NumberToWords {
	
	public String convert();
	
}
