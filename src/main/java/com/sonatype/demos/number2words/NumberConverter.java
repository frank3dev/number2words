package com.sonatype.demos.number2words;

import java.util.AbstractMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class NumberConverter {
	
	public static final String INPUT_ERROR_MSG = "The input appears invalid, please try again.";

	protected Map<String, String[]> _words = Stream
			.of(new AbstractMap.SimpleEntry<>("unitWords",
					new String[] { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
							"ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen",
							"eighteen", "nineteen" }),
					new AbstractMap.SimpleEntry<>("tenWords",
							new String[] { "", "", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty",
									"ninety" }),
					new AbstractMap.SimpleEntry<>("scaleWords", new String[] { "hundred", "thousand", "million", "billion" }))
			.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));
	
	/**
	 * Converts an int to words
	 * @param number the number to convert
	 * @param isLastUnit to append and word
	 * @return the number in words
	 */
	protected String convertThreeDigitsToWords(int number, boolean isLastUnit) {
		
		StringBuilder groupWords = new StringBuilder();

		// get the hundreds
		int hundreds = number / 100;
		int tensUnits = number % 100;
		if (hundreds != 0) {
			groupWords.append(_words.get("unitWords")[hundreds]);
			groupWords.append(" ");
			groupWords.append(_words.get("scaleWords")[0]);
			if (tensUnits != 0 && isLastUnit) {
				groupWords.append(" and");
			}
		}

		// get the tens and units
		int tens = tensUnits / 10;		
		int units = tensUnits % 10;
		if(hundreds != 0 && tensUnits != 0) {
			groupWords.append(" ");
		}
		if (tens >= 2) {
			groupWords.append(_words.get("tenWords")[tens]);
			if (units != 0) {
				groupWords.append(" ");
				groupWords.append(_words.get("unitWords")[units]);
			}
		} else if (tensUnits != 0) {
			groupWords.append(_words.get("unitWords")[tensUnits]);
		}		

		return groupWords.toString();
	}
	
}
