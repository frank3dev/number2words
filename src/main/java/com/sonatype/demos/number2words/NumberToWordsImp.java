package com.sonatype.demos.number2words;

import java.math.BigInteger;
import java.util.regex.Pattern;

public class NumberToWordsImp extends NumberConverter implements NumberToWords {

	public static final String NEGATIVE_WORD = "negative";
	private BigInteger number;
	private boolean inputError;

	public NumberToWordsImp(String numberStr) {
		try {
			this.number = verifyInputValue(numberStr);
		} catch (NumberFormatException nfe) {
			inputError = true;
		}
	}

	public NumberToWordsImp(Integer number) {
		this.number = BigInteger.valueOf(number);
	}

	/**
	 * Converts the number to words 
	 * @return a String with the words representations
	 */
	public String convert() {
		if (inputError) {
			return INPUT_ERROR_MSG;
		}

		StringBuilder sbResponse = new StringBuilder();

		// Zero validation
		if (number.intValue() == 0) {
			sbResponse.append(_words.get("unitWords")[0]);
		} else {
			// Negative validation
			if (number.intValue() < 0) {
				sbResponse.append(NEGATIVE_WORD).append(" ");
				number = number.abs();
			}

			int[] threeDigitsGroup = getThreeDigitsGroup();
			for (int i = 3; i >= 0; i--) {
				if (threeDigitsGroup[i] > 0) {
					String numberConverted = convertThreeDigitsToWords(threeDigitsGroup[i], i == 0 ? true : false);
					// Append "and" word
					if (i == 0 && threeDigitsGroup[0] < 100 && number.intValue() > 100) {
						sbResponse.append("and ");
					}
					sbResponse.append(numberConverted);
					// add scale word, the hundred is already assigned
					if (i > 0) {
						sbResponse.append(" ");
						sbResponse.append(_words.get("scaleWords")[i]);
					}
					sbResponse.append(" ");
				}
			}
			// Remove last empty char
			sbResponse.deleteCharAt(sbResponse.length() - 1);
		}

		// Capitalize the first word
		sbResponse.replace(0, 1, sbResponse.substring(0, 1).toUpperCase());

		return sbResponse.toString();
	}

	/**
	 * @return an array with groups of three digits
	 */
	private int[] getThreeDigitsGroup() {
		BigInteger modulusNumber = this.number;
		int[] threeDigitsGroup = new int[4];
		for (int i = 0; i < 4; i++) {
			threeDigitsGroup[i] = modulusNumber.mod(new BigInteger("1000")).intValue();
			modulusNumber = modulusNumber.divide(new BigInteger("1000"));
		}

		return threeDigitsGroup;
	}

	/**
	 * Validates the input, accepts Integers, Negatives and could be separated by
	 * comma like 1,000,000
	 * @param input the number
	 * @return a valid number to convert
	 */
	private BigInteger verifyInputValue(String input) throws NumberFormatException {
		Pattern pattern = Pattern.compile("^-?(?:\\d+|\\d{1,3}(?:,\\d{3})+)?$");
		if (pattern.matcher(input).matches()) {
			return BigInteger.valueOf(Integer.parseInt(input.replaceAll("\\,", "")));
		} else {
			throw new NumberFormatException();
		}
	}

	public static void main(String[] args) {
		System.out.println(new NumberToWordsImp(args[0]).convert());
	}

}
