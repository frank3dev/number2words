package com.sonatype.demos.number2words;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.stream.Stream;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.junit.jupiter.params.provider.ValueSource;

class NumberToWordsImpTest {

	@SuppressWarnings("unused")
	private static Stream<Arguments> provideValidIntegerValues() {
		return Stream.of(Arguments.of(0, "Zero"),
				// 010 int is parsed as an octal number
				Arguments.of(010, "Eight"),
				Arguments.of(50, "Fifty"), 
				Arguments.of(-1001, "Negative one thousand and one"),
				Arguments.of(10583, "Ten thousand five hundred and eighty three"),
				Arguments.of(100102, "One hundred thousand one hundred and two"),
				Arguments.of(456689, "Four hundred fifty six thousand six hundred and eighty nine"),
				Arguments.of(2345678, "Two million three hundred forty five thousand six hundred and seventy eight"),
				Arguments.of(19345678,
						"Nineteen million three hundred forty five thousand six hundred and seventy eight"),
				Arguments.of(Integer.MAX_VALUE,
						"Two billion one hundred forty seven million four hundred eighty three thousand six hundred and forty seven"),
				Arguments.of(Integer.MIN_VALUE,
						"Negative two billion one hundred forty seven million four hundred eighty three thousand six hundred and forty eight"));
	}

	@SuppressWarnings("unused")
	private static Stream<Arguments> provideValidStringValues() {
		return Stream.of(Arguments.of("0", "Zero"), 
				Arguments.of("010", "Ten"),
				Arguments.of("1", "One"),
				Arguments.of("100", "One hundred"),
				Arguments.of("-1000", "Negative one thousand"),
				Arguments.of("456,689", "Four hundred fifty six thousand six hundred and eighty nine"),
				Arguments.of("1000100", "One million one hundred"),
				Arguments.of("2147483647",
						"Two billion one hundred forty seven million four hundred eighty three thousand six hundred and forty seven"),
				Arguments.of("-2,147,483,648",
						"Negative two billion one hundred forty seven million four hundred eighty three thousand six hundred and forty eight"));
	}

	@ParameterizedTest
	@MethodSource("provideValidIntegerValues")
	void convertShouldRetunValidOutputWithInteger(int input, String expected) {
		NumberToWords ntw = new NumberToWordsImp(input);
		assertEquals(ntw.convert(), expected);
	}

	@ParameterizedTest
	@MethodSource("provideValidStringValues")
	void convertShouldRetunValidOutputWithString(String input, String expected) {
		NumberToWords ntw = new NumberToWordsImp(input);
		assertEquals(ntw.convert(), expected);
	}

	@ParameterizedTest
	@ValueSource(strings = { "1-000", "100-", "10,00", "1,000,0", "2147483649", "-2,147,483,649" })
	void convertShouldRetunInvalidOutputWithInteger(String input) {
		NumberToWords ntw = new NumberToWordsImp(input);
		assertEquals(ntw.convert(), NumberConverter.INPUT_ERROR_MSG);
	}
}
